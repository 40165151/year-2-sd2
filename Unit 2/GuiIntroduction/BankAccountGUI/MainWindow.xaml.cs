﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BankAccountGUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private bool valid;
        private double balance = 0.00;
        private double input;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void txtbx_Amount_TextChanged(object sender, TextChangedEventArgs e)
        {
            string tb = txtbx_Amount.Text;
            if (double.TryParse(tb, out input))
            {
                if (input > 0)
                {
                    input = Math.Round(input, 2);
                    valid = true;
                }
                else
                {
                    valid = false;
                    System.Windows.MessageBox.Show("ERROR: Please enter a number above 0");
                }
            }
            else
            {
                System.Windows.MessageBox.Show("ERROR: Please enter a valid rational number");
                valid = false;
            }
        }

        private void btn_Credit_Click(object sender, RoutedEventArgs e)
        {
            if (valid)
            {
                if (balance + input <= 49373938500000)
                {
                    balance += input;
                    lstBx_Log.Items.Add("£" + input + " deposited, balance = £" + balance);
                }
                else
                    System.Windows.MessageBox.Show("ERROR: You cant excede the ammount of money in the world. Which is basically about 75 trillion dollars but that's a rather abstract figure because it's rather difficult to calculate all the money in the world. Though it may be obvious by now that the only reason this limit is in place is because I know someone is going to try to go over the double capacity.");
                txt_Balance.Content = "Your current Balance is £" + balance;
            }
        }

        private void btn_Debit_Click(object sender, RoutedEventArgs e)
        {
            if (valid)
            {
                if (balance - input >= 0)
                {
                    balance -= input;
                    lstBx_Log.Items.Add("£" + input + " withdrawn, balance = £" + balance);
                }
                else
                    System.Windows.MessageBox.Show("ERROR: Amount requested excedes amount present");
                txt_Balance.Content = "Your current Balance is £" + balance;
            }
        }
    }
}
