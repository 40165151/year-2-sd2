﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Students_Records_System
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btn_Address_Click(object sender, RoutedEventArgs e)
        {
            new AddressWindow().ShowDialog();
        }

        private void btn_Quit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btn_Save_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(txtbx_Name.Text) && !string.IsNullOrWhiteSpace(txtbx_Matriculation.Text))
            {
                MessageBox.Show("New student details being added");
            }
            else
                MessageBox.Show("ERROR: Please fill out all fields.");
        }
    }
}
