﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AddNumbers
{
    class Program
    {
        static void Main(string[] args)
        {
            int total = 0;
            string input;
            char[] delimiterChars = {','};
            while (true)
            {
                Console.WriteLine("Please enter a numbers separated by a comma(2, 5, 3...)");
                input = Console.ReadLine();
                if(!System.Text.RegularExpressions.Regex.IsMatch(input, "^([0-9]+, )*[0-9]+$"))
                {
                    Console.WriteLine("ERROR: Invalid format");
                }
                else
                    break;
            }
            string[] numberArray = input.Split(delimiterChars);
            for(int i = 0; i < numberArray.Length; i++)
            {
                total += Convert.ToInt32(numberArray[i]);
            }
            Console.WriteLine(total);
        }
    }
}
