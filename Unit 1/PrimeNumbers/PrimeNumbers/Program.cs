﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrimeNumbers
{
    class Program
    {
        static void Main(string[] args)
        {
            bool primeFlag = true;
            int checkPrime;
            while (true)
            {
                Console.WriteLine("Enter a number between 1 and 10000");
                string input = Console.ReadLine();
                if (!int.TryParse(input, out checkPrime))
                {
                    Console.WriteLine("ERROR: THAT WAS NOT A VALID NUMBER");
                }
                else
                {
                    if(checkPrime > 0 && checkPrime <= 10000)
                    {
                        break;
                    }
                    else
                        Console.WriteLine("ERROR: NUMBER IS OUT OF RANGE");
                }
            }

            for (int i = 1; i <= checkPrime; i++)
            {
                for (int j = 2; j < 8; j++)
                {
                    if (i != j)
                    {
                        if (i % j == 0)
                        {
                            primeFlag = false;
                        }
                    }
                }
                if (primeFlag == true)
                {
                    Console.Write(i+" ,");
                }
                else
                    primeFlag = true;
            }
        
        }
    }
}